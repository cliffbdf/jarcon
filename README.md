# Jar Consolidator

Combine the class files from all your Jars and other sources - but only those that
are actually needed at runtime.

Create Java microservices that are truly micro!

## Why

The Java community uses Maven for obtaining dependencies when building a project.
Unfortunately, in this approach, dependencies are tracked at a project level, which is
not very granular: most projects use tens of other projects, but only few pieces of
those; yet Maven will build your project by including every class in every Jar file
of every dependent project. As a result, a project that only needs, say, 30K of Java
class files ends up with a deployment that consists of 800Mb of Jar files from other
projects. Thus, if one is building a "microservice" using Java, it is hard to actually
be very "micro".

JarCon determines which classes your app actually needs, and builds a Jar file that
contains only those. You then deploy only that Jar file. Thus, you can reduce your
deployment footprint by a factor of 10.

The [Maven shade plugin](https://maven.apache.org/plugins/maven-shade-plugin/examples/includes-excludes.html)
has a switch to remove classes that are not actually used, but does not provide fine
grained control. E.g., if your project uses reflection, there is no way, using shade,
to determine the dependencies that result from the classes that are loaded by reflection.
Jarcon provides this fine grained control, by allowing you to specify all of the root
classes - so you can include classes that are called by reflection, and then their
dependencies will be included as well.

The Java 9+ module system, through the jlink tool, supports the ability to create a
deployment that includes only the modules that an application needs. However, this is
still pretty large grain, and the reduction in Java class file footprint is likley to
be a factor of 2 or so, whereas if one removes unneeded classs, one can reduce the
footprint by a factor of 10,000 - yes, ten thousand - although because the JVM and
OS are large, the overall container image is likly to be ten times smaller rather than
10,000 times smaller.

## Maven with Java 9 and later

When using Maven with Java 9 or later, make sure to run Maven itself with Java 8 or earlier,
because Maven appears to require tools.jar, which is not included after Java 8.
You can do this by setting JAVA_HOME for the process that runs Maven.

To compile with Java 9 and later, and use jarcon on the resulting Jars, I recommend
using the Maven toolchain feature, and setting source and target attributes of
the maven compiler plugin to specify the Java version.

TBD:
If your applications are defined as Java modules, jarcon will extract the needed
classes from each module, and write all of them to a new module.

Here are some references pertaining to running Maven with Java 9 and later:

* https://blog.codefx.org/tools/maven-on-java-9/
* https://cwiki.apache.org/confluence/display/MAVEN/Java+9+-+Jigsaw
* http://maven.apache.org/surefire/maven-surefire-plugin/java9.html

## Usage

The main component used by jarcon is the dependency analyzer (CDA) available from
http://www.dependency-analyzer.org/index.html. Unfortunately, this component is not
available from a maven repository. You will need to download those and put them in
a directory, which I will refer to as <i>cda-jar-dir</i>.

### Usage from the command line

If running from a command line you need to obtain the jarcon Jar file, from
http://repo1.maven.org/maven2/com/cliffberg/jarcon/1.0.0/jarcon-1.0.0.jar
and the Programmer's Friend Jar files, obtained from
http://www.programmers-friend.org/download/

....TBD - push to maven central. See
https://maven.apache.org/repository/guide-central-repository-upload.html

Here is an example of running the jarcon program:

<pre>
	java -cp <i>jarcon-jar</i>:<i>cda-jar-dir</i>/* com.cliffberg.jarcon.JarConsolidator \
		--jarPath="${TargetJarClassPath}" \
		--rootClassess="${RootClassNames}" \
		--properties="com/abc/SomeDatabaseDriver.properties" \
		--targetJarPath=output.jar \
		--manifestVersion="1.0.0" \
		--createdBy="John Smith"
</pre>

As you can see, when running jarcon, your classpath must specify,

<dl>
<dt>jarcon-jar</dt><dd>The jarcon jar file</dd>
<dt>cda-jar-dir</dt><dd>The directory containing the CDA Jars.</dd>
</dl>


These are the arguments:

<dl>
<dt>--verbose</dt><dd>Optional.</dd>

<dt>--jarPath</dt><dd>Any valid Java classpath, containing the Jars and classfiles that your
application needs.</dd>

<dt>--rootClassess</dt><dd>One or more comma-separated class file names.
These are analyzed for dependencies; only the classes needed by your root classes, directly
or indirectly, are included in the resulting Jar. If your app calls classes by reflection,
you should list those classes here as well.</dd>

<dt>--properties</dt><dd>Optional. One or more comma-separated property file paths.</dd>

<dt>--targetJarPath</dt><dd>The file path of the Jar file that is to be produced. This is
what you can include when deploying your application.</dd>

<dt>--manifestVersion</dt><dd>The manifestVersion field to write to the consolidated Jar file.</dd>

<dt>--createdBy</dt><dd>The createdBy field to write to the consolidated Jar file.</dd>
</dl>

### Usage from maven

There is no maven plugin for jarcon, so simply run it using the exec-maven-plugin plugin.
For example,

```
	<plugin>
		<artifactId>exec-maven-plugin</artifactId>
		<version>1.6.0</version>
		<groupId>org.codehaus.mojo</groupId>
		<executions>
			<execution>
				<id>Running jarcon</id>
				<phase>package</phase>
				<goals>
					<goal>exec</goal>
				</goals>
				<configuration>
					<executable>bash</executable>
					<commandlineArgs>java ....</commandlineArgs>
				</configuration>
			</execution>
		</executions>
	</plugin>
```

## Including classes that are called by reflection


## Including Java property files



