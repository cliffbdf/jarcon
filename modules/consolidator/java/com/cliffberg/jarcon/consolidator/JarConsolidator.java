/*
 * Copyright (c) by Cliff Berg, 2018.
 * Open source: subject to Apache 2.0 license.
 * https://www.apache.org/licenses/LICENSE-2.0
 */

package com.cliffberg.jarcon.consolidator;

import org.pfsw.tools.cda.base.model.*;
import org.pfsw.tools.cda.base.model.workset.*;
import org.pfsw.tools.cda.core.processing.IProgressMonitor;
import org.pfsw.tools.cda.core.init.WorksetInitializer;
import org.pfsw.tools.cda.core.dependency.analyzer.DependencyAnalyzer;
import org.pfsw.tools.cda.core.dependency.analyzer.model.DependencyInfo;
import org.pfsw.tools.cda.core.processing.IProgressMonitor;

import org.pfsw.tools.cda.core.dependency.analyzer.model.ClassDependencyInfo;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.jar.JarEntry;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Properties;
import java.util.Date;
import java.util.Enumeration;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLClassLoader;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * For creating a "consolidated" Jar file that combines a set of class file sources,
 * but only containing the classes that are actually needed.
 */
public class JarConsolidator {
	
	public static void main(String[] args) throws Exception {
		
		boolean verbose = false;
		String[] jarPaths = null;
		String[] rootClassNames = null;
		String targetJarPath = null;
		String[] propertieFilePaths = null;
		String manifestVersion = null;
		String createdBy = null;

		OptionParser parser = new OptionParser();
		parser.accepts("verbose");
		parser.accepts("jarPath",
			"Any valid Java classpath, containing the Jars and classfiles that your application needs.")
			.withRequiredArg().required();
		parser.accepts("rootClasses",
			"One or more comma-separated class file names. These are analyzed " +
			"for dependencies; only the classes needed by your root classes, directly " +
			"or indirectly, are included in the resulting Jar. If your app calls " +
			"classes by reflection, you should list those classes here as well.")
			.withRequiredArg().required();
		parser.accepts("targetJarPath",
			"The file path of the Jar file that is to be produced. This is what you " +
			"can include when deploying your application.")
			.withRequiredArg().required();
		parser.accepts("properties",
			"Optional. One or more comma-separated property file paths.")
			.withRequiredArg();
		parser.accepts("manifestVersion",
			"The manifestVersion field to write to the consolidated Jar file.")
			.withRequiredArg();
		parser.accepts("createdBy",
			"The createdBy field to write to the consolidated Jar file.")
			.withRequiredArg();
		parser.acceptsAll(Arrays.asList( "h", "help", "?" ), "show help" ).forHelp();
		
		OptionSet options = null;
		try {
			options = parser.parse(args);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			parser.printHelpOn(System.out);
			System.exit(1);
		}
		
		if (options.has("verbose")) {
			if (options.hasArgument("verbose")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			verbose = true;
		}
		
		if (options.has("jarPath")) {
			if (! options.hasArgument("jarPath")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			String jarPathString = (String)(options.valueOf("jarPath"));
			jarPaths = jarPathString.split(":");
		}
		
		if (options.has("rootClasses")) {
			if (! options.hasArgument("rootClasses")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			String rootClassString = (String)(options.valueOf("rootClasses"));
			rootClassNames = rootClassString.split(",");
		}
		
		if (options.has("targetJarPath")) {
			if (! options.hasArgument("targetJarPath")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			targetJarPath = (String)(options.valueOf("targetJarPath"));
		}
		
		if (options.has("properties")) {
			if (! options.hasArgument("properties")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			String propertyString = (String)(options.valueOf("properties"));
			propertieFilePaths = propertyString.split(",");
		}
		
		if (options.has("manifestVersion")) {
			if (! options.hasArgument("manifestVersion")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			manifestVersion = (String)(options.valueOf("manifestVersion"));
		}
		
		if (options.has("createdBy")) {
			if (! options.hasArgument("createdBy")) {
				parser.printHelpOn(System.out);
				System.exit(1);
			}
			createdBy = (String)(options.valueOf("createdBy"));
		}
		
		(new JarConsolidator(verbose, manifestVersion, createdBy)).
			createConsolidatedJar(jarPaths, rootClassNames, propertieFilePaths, targetJarPath);
	}
	
	private boolean verbose = false;
	private String manifestVersion;
	private String createdBy;
	
	public JarConsolidator(boolean verbose, String manifestVersion, String createdBy) {
	
		this.verbose = verbose;
		this.manifestVersion = manifestVersion;
		this.createdBy = createdBy;
	}

	/**
	 * Create a consolidated Jar file.
	 */
	public void createConsolidatedJar(String[] jarPaths, String[] rootClassNames,
		String[] propertieFilePaths, String targetJarPath) throws Exception {
		
		Workset workset = new Workset("Sample1");
		Map<String, ClassInformation> classMap = new HashMap<String, ClassInformation>();
		
		for (String rootClassName : rootClassNames) {
			ClassInformation[] cis = determineDependencies(workset, jarPaths, rootClassName);
			for (ClassInformation ci : cis) {
				String className = ci.getName();
				ClassInformation ci2 = classMap.get(className);
				if (ci2 == null) {
					classMap.put(ci.getName(), ci);  // prevents duplicates
				} else {
					long size1 = ci.getFileSize();
					long size2 = ci2.getFileSize();
					if (size1 != size2) {
						System.out.println("Warning: two sources for " + className +
						" and they are different sizes");
					}
				}
			}
		}
		
		// Add root classes to class map.
		for (String rootClassName : rootClassNames) {
			ClassInformation classInfo = workset.getClassInfo(rootClassName);
			classMap.put(classInfo.getName(), classInfo);
		}
		
		writeToJar(classMap, jarPaths, propertieFilePaths, targetJarPath);
		workset.release();
	}
	
	/**
	 * Determine the set of classes that depend on the specified root class.
	 */
	public ClassInformation[] determineDependencies(Workset workset,
		String[] jarPaths, String rootClassName) throws Exception {
		
		if (this.verbose) System.out.println("Determiningn dependencies for " +
			rootClassName);
		
		for (String jarPath : jarPaths) {
			ClasspathPartDefinition partDefinition = new ClasspathPartDefinition(jarPath);
			workset.addClasspathPartDefinition(partDefinition);
		}
		
		WorksetInitializer wsInitializer = new WorksetInitializer(workset);
		IProgressMonitor monitor = null;
		wsInitializer.initializeWorksetAndWait(monitor);
		
		ClassInformation classInfo = workset.getClassInfo(rootClassName);
		DependencyAnalyzer analyzer = new DependencyAnalyzer(classInfo, new IProgressMonitor() {
			public boolean showProgress(int value, java.lang.Object... info) {
				if (JarConsolidator.this.verbose) System.out.println(value + " %; " + info.toString() + "...");
				else System.out.print(".");
				return true;  // true means continue
			}
			public void startProgressMonitor() {
				System.out.print("Starting analysis");
			}
			public void terminateProgressMonitor() {
				System.out.println("analysis complete.");
			}
		});
		analyzer.analyze();
		DependencyInfo result = analyzer.getResult();
		ClassInformation[] classes = result.getAllReferredClasses(); 
		
		System.out.println();
		System.out.println(classInfo.getName() + " dependes on " + classes.length + " classes");
		return classes;
	}
	
	/**
	 * Add each class to the target jar file.
	 */
	public void writeToJar(Map<String, ClassInformation> classMap, String[] jarPaths,
		String[] propertieFilePaths, String targetJarPath) throws Exception {

		Manifest manifest = new Manifest();
		Attributes global = manifest.getMainAttributes();
		if (this.manifestVersion != null) {
			global.put(Attributes.Name.MANIFEST_VERSION, this.manifestVersion);
		}
		if (this.createdBy != null) {
			global.put(new Attributes.Name("Created-By"), this.createdBy);
		}
		OutputStream os = new FileOutputStream(targetJarPath);
		JarOutputStream jos = new JarOutputStream(os, manifest);
		byte[] buffer = new byte[1024];
		
		// Write class files.
		
		int numClassFilesWritten = 0;
		for (String className : classMap.keySet()) {
						
			ClassInformation classInformation = classMap.get(className);
			
			// Construct file path of the new entry, relative to the jar root.
			String classFileLocalPath = classInformation.getClassFileName();
			if (this.verbose) System.out.println("classFileLocalPath=" + classFileLocalPath);
			
			// Obtain location of the original class file.
			ClassContainer container = classInformation.getClassContainer();
			if (container == null) {
				// We can skip - class is in Java runtime.
				if (this.verbose) System.out.println("\tContainer for " + classInformation.getName() + " is null");
			} else {
				
				String folderName = container.getClassesFolderName(); // usually ""
				String containerPath = container.getPhysicalName();  // jar file path
				
				if (this.verbose) System.out.println("\tfolderName=" + folderName);
				if (this.verbose) System.out.println("\tcontainerPath=" + containerPath);
				
				long lastModifiedMs;
				
				InputStream is = null;
				if (container.isPhysicalArchive()) {
					if (this.verbose) System.out.println("\tcontainer is physical archive");
					JarFile inputJarFile = new JarFile(containerPath);
					String inputEntryName = classFileLocalPath;
					ZipEntry entry = new ZipEntry(inputEntryName);
					lastModifiedMs = entry.getTime​();
					is = inputJarFile.getInputStream(entry);
				} else if (container.isPhysicalDirectory()) {
					
					//is = new BufferedInputStream(new FileInputStream(....));
					throw new Exception("Not implemented");
				} else throw new RuntimeException(
					"Unexpected container for class file: " + container.getClass().getName());
				
				writeJarEntry(is, jos, classFileLocalPath, lastModifiedMs, buffer) ;
				numClassFilesWritten++;
			}
		}
		
		// Write property files.
		
		URL[] jarURLs = new URL[jarPaths.length];
		for (int i=0; i<jarPaths.length; i++) {
			jarURLs[i] = new URL("file://" + jarPaths[i]);
			System.out.println("URL: " + jarURLs[i].toString());  // debug
		}
		URLClassLoader propertyClassLoader = new URLClassLoader(jarURLs);
		
		int numPropFilesWritten = 0;
		for (String propertyFilePath : propertieFilePaths) {
			
			System.out.println("Loading " + propertyFilePath);  // debug
			URL url = propertyClassLoader.findResource(propertyFilePath);
			if (url == null) {
				System.out.println("Warning: unable to locate resource " +
					propertyFilePath);
				continue;
			}
			long lastMod = 0;
			URLConnection con;
			try {
				con = url.openConnection();
				lastMod = con.getLastModified();
			}
			catch (Exception ex) {
				System.out.println("Warning: unable to open connection to resource " +
					propertyFilePath);
				continue;
			}
			
			Properties prop = new Properties();
			
			InputStream is2 = propertyClassLoader.getResourceAsStream(propertyFilePath);
			writeJarEntry(is2, jos, propertyFilePath, lastMod, buffer);
			numPropFilesWritten++;
		}
		
		jos.flush();
		jos.close();
		System.out.println(numClassFilesWritten + " class files and " +
			numPropFilesWritten + " property files written to " + targetJarPath);
	}
	
	public void writeJarEntry(InputStream is, JarOutputStream jos,
		String localPath, long lastModifiedMs, byte[] buffer)throws Exception  {
		
		// Create a new Jar file entry.
		JarEntry je = new JarEntry(localPath);
		je.setTime(lastModifiedMs);
		je.setMethod​(ZipEntry.DEFLATED);
		jos.putNextEntry(je);
		
		// Write the content of the entry.
		if (this.verbose) System.out.println("Writing " + localPath);
		int len = 0;
		while((len = is.read(buffer, 0, buffer.length)) != -1) {
			jos.write(buffer, 0, len);
		}
		
		is.close();
		jos.closeEntry();
	}
}
